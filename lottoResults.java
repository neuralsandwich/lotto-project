import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;




public class lottoResults {

       final ExecutorService executorService = Executors.newFixedThreadPool(16);

       private static String filePath = "/home/neuralsandwich/lotto-results.csv";
       private static final int POOLSIZE = 49;
       private static final int DRAWSIZE = 5;


	// ArrayLists of all the draws, balls and ball Pairs
	public static ArrayList<Draw> LottoDraws = new ArrayList<Draw>();
	public static ArrayList<Ball> balls = new ArrayList<Ball>();
	public static ArrayList<Ball> bonus = new ArrayList<Ball>();
	public static ArrayList<Numbers> myNumbers = new ArrayList<Numbers>();


       /* Occurrences ()
	 * Counts the frequency of which the numbers
	 * appear in the lottery draws
	 */
	public static void Occurances() {
		for (int i=0;i<=(LottoDraws.size()-1);i++) {

			int [] a = LottoDraws.get(i).getDraw();

                       for (int j=0;j<=DRAWSIZE;j++) {
				balls.get(a[j]-1).incrementFreq();
			}

		}
	} // Occurrences()

	/*
	 * MostOccuring()
	 * Outputs the most occurring single balls
	 */
	@SuppressWarnings("unchecked")
	public static void MostOccuring() {
		Collections.sort(balls);
		Collections.sort(bonus);
		System.out.println("\nMost Occuring:");

		int [] a = new int [6];
		for (int i=0;i<=DRAWSIZE;i++) {
			System.out.print(balls.get(balls.size()-(i+1)) + ", ");
			a[i]=balls.get(balls.size()-(i+1)).getNum();
		}
		System.out.println();
		Arrays.sort(a);
		for (int i=0;i<=DRAWSIZE;i++) {
			System.out.print(a[i] + ",");
		}

		System.out.println();
		System.out.println("Bonus ball:");
		for (int i=0;i<=DRAWSIZE;i++) {
			System.out.print(bonus.get(bonus.size()-(i+1)) + ",");
		}

		System.out.println();

		myNumbers.add(new Numbers(a));
		//System.out.println(myNumbers.size());


	} // MostOccuring()

	/*
	 * pairs()
	 * Searches the LottoDraws ArrayList
	 * for the most occurring number pairs
	 */
	@SuppressWarnings("unchecked")
	public static void pairs() {

		System.out.println("Most Occuring Pairs:");

		ArrayList<Pair> Pairs = new ArrayList<Pair>();
		// Two loops used to create the number pairs
		for (int i=1; i<POOLSIZE;i++) {
			for (int j=i+1;j<POOLSIZE;j++) {
				Pairs.add(new Pair(i,j));
				//System.out.println(Pairs.get(Pairs.size()-1));
				for (int listIndex=0; listIndex < LottoDraws.size();listIndex++) {
					//System.out.println(LottoDraws.get(listIndex));
					int [] curDraw = LottoDraws.get(listIndex).getDraw();
					/*Using boolean flags to
					 * make sure both numbers
					 * are in the same draw */
					boolean pair1 = false;
					boolean pair2 = false;
					for(int k=0;k<=DRAWSIZE;k++) {
						if (curDraw[k] == i) {
							pair1 = true;
						}
						if (curDraw[k] == j) {
							pair2 = true;
						}
					}
					// If the pair has occurred then
					// it is incremented in the pairGrid
					if (pair1 == true && pair2 == true) {
						Pairs.get(Pairs.size()-1).incrementOccurrance();
						//System.out.println(Pairs.get(Pairs.size()-1));
					}
				}
				//System.out.println(Pairs.get(Pairs.size()-1));
			}
		}

		// Pairs are sorted after being added to the
		// the Pairs ArrayList
		Collections.sort(Pairs);

		// Adds the most occurring pair to Hashset
		// Makes sure that all numbers are unique
		Set<Integer> s = new HashSet<Integer>();
		s.add(Pairs.get(Pairs.size()-1).getNum1());
		s.add(Pairs.get(Pairs.size()-1).getNum2());
		//System.out.println("asdf" + Pairs.get(Pairs.size()-1));

		for (int i=1;i<=3;i++) {
			System.out.print(Pairs.get(Pairs.size()-i) + ", ");
		}

		//System.out.println("asdf" + Pairs.get(Pairs.size()-1));

		int i=1; int c=0;
		Integer [] p = new Integer [2];
		while (c<=1) {
			p[0] = Pairs.get(Pairs.size()-i).getNum1();
			p[1] = Pairs.get(Pairs.size()-i).getNum2();
			if (!(s.contains(p[0])) && !(s.contains(p[1]))) {
				s.add(p[0]);
				s.add(p[1]);
				c++;
				//System.out.print("asdf " + p[0] + "," + p[1] + ",");
			}
			i++;
		}

		// Once the three unique pairs are found
		// They are added to a temporary ArrayList 
		// of Integers for sorting for output
		ArrayList<Integer> l = new ArrayList<Integer>(s);

		// ArrayList is sorted then printed to console
		Collections.sort(l);
		System.out.println();
		int [] a = new int [6];
		for (int m=0;m<=DRAWSIZE;m++) {
			System.out.print(l.get(m)+",");
			a[m]=l.get(m);
		}
		System.out.println();
		//System.out.println();

		myNumbers.add(new Numbers(a));

		Pairs=null;
	} // pairs()

	/*
	 * Triplets()
	 * Searches the LottoDraws ArrayList
	 * for the most occurring number triplets
	 */
	@SuppressWarnings("unchecked")
	public static void Triplets() {

		ArrayList<Triplet> Triplets = new ArrayList<Triplet>();
		// Three loops used to create the number triplets
		for (int i=1; i<POOLSIZE;i++) {
			for (int j=i+1;j<POOLSIZE;j++) {
				for (int k=j+1;k<POOLSIZE;k++) {
					Triplets.add(new Triplet(i,j,k));
					//System.out.println(Triplets.get(Triplets.size()-1));
					for (int listIndex=0; listIndex < LottoDraws.size();listIndex++) {
						int [] curDraw = LottoDraws.get(listIndex).getDraw();
						/*Using boolean flags to
						 * make sure all three
						 *  numbers are in the 
						 *  same draw */
						boolean triplet1 = false;
						boolean triplet2 = false;
						boolean triplet3 = false;
						for(int l=0;l<=DRAWSIZE;l++) {
							if (curDraw[l] == i) {
								triplet1 = true;
							}
							if (curDraw[l] == j) {
								triplet2 = true;
							}
							if (curDraw[l] == k) {
								triplet3 = true;
							}
						}
						// if the Triplet are in the draw then increment it
						// in the tripletCube
						if (triplet1 == true && triplet2 == true && triplet3 == true) {
							Triplets.get(Triplets.size()-1).incrementOccurrance();
							//System.out.println(Triplets.get(Triplets.size()-1));
						}
					}
				}
			}
		}

		// The ArrayList is sorted
		Collections.sort(Triplets);

		System.out.println("Most Occuring Triplets:");

		System.out.print(Triplets.get(Triplets.size()-1).getNum1()+" " );
		System.out.print(Triplets.get(Triplets.size()-1).getNum2()+" " );
		System.out.print(Triplets.get(Triplets.size()-1).getNum3()+" ("+Triplets.get(Triplets.size()-1).getOccurance()+")," );



		// The most occuring triplets are added to
		// this set to make sure they are unique
		int [] t1 = new int [3]; int [] t2 = new int [3]; 
		int [] t3 = new int [3]; int[]  t4 = new int [3];
		//int [] t5 = new int [3]; int[]  t6 = new int [3];
		Set<Integer> s = new HashSet<Integer>();
		s.add(Triplets.get(Triplets.size()-1).getNum1());
		s.add(Triplets.get(Triplets.size()-1).getNum2());
		s.add(Triplets.get(Triplets.size()-1).getNum3());
		t1[0] = Triplets.get(Triplets.size()-1).getNum1();
		t1[1] = Triplets.get(Triplets.size()-1).getNum2();
		t1[2] = Triplets.get(Triplets.size()-1).getNum3();
		
		int i=2; int count=0;
		Integer [] t = new Integer [3];

		while (count<3) {
			t[0] = Triplets.get(Triplets.size()-i).getNum1();
			t[1] = Triplets.get(Triplets.size()-i).getNum2();
			t[2] = Triplets.get(Triplets.size()-i).getNum3();
			if ( !(s.contains(t[0])) && !(s.contains(t[1]))  && !(s.contains(t[2]))) {
				s.add(t[0]);
				s.add(t[1]);
				s.add(t[2]);
				count++;
				//System.out.println(c);
				switch (count) {
				case 1:
					t2[0] = Triplets.get(Triplets.size()-i).getNum1();
					t2[1] = Triplets.get(Triplets.size()-i).getNum2();
					t2[2] = Triplets.get(Triplets.size()-i).getNum3();
				case 2:
					t3[0] = Triplets.get(Triplets.size()-i).getNum1();
					t3[1] = Triplets.get(Triplets.size()-i).getNum2();
					t3[2] = Triplets.get(Triplets.size()-i).getNum3();
				case 3:
					t4[0] = Triplets.get(Triplets.size()-i).getNum1();
					t4[1] = Triplets.get(Triplets.size()-i).getNum2();
					t4[2] = Triplets.get(Triplets.size()-i).getNum3();
				default :
				}
				System.out.print(Triplets.get(Triplets.size()-i).getNum1()+" " );
				System.out.print(Triplets.get(Triplets.size()-i).getNum2()+" " );
				System.out.print(Triplets.get(Triplets.size()-i).getNum3()+" ("+Triplets.get(Triplets.size()-i).getOccurance()+")," );
			}
			i++;
		}
		System.out.println();

		
		// Sorts the arrays in to ascending order then outputs them
		int [] a = new int [6];
		for (int  j=0; j<=2; j++) {
			a[j]=t1[j];
			a[j+3]=t2[j];
		}
		int [] b = new int [6];
		for (int  j=0; j<=2; j++) {
			b[j]=t3[j];
			b[j+3]=t4[j];
		}

		Arrays.sort(a);
		Arrays.sort(b);
		for (int m=0;m<=DRAWSIZE;m++) {
			System.out.print(a[m]+",");
		}
		System.out.println();
		for (int m=0;m<=DRAWSIZE;m++) {
			System.out.print(b[m]+",");
		}
		System.out.println();

		myNumbers.add(new Numbers(a));
		myNumbers.add(new Numbers(b));

		Triplets=null;
	} // Triplets()

	/*
	 * Quadruplets()
	 * Searches the LottoDraws ArrayList
	 * for the most occurring number Quadruplets
	 */
	@SuppressWarnings("unchecked")
	public static void Quadruplets() {

		ArrayList<Quadruplet> Quadruplets = new ArrayList<Quadruplet>();
		System.out.println("Most Occuring Quadruplet:");

		// Three loops used to create the number triplets
		for (int i=1; i<POOLSIZE;i++) {
			for (int j=i+1;j<POOLSIZE;j++) {
				for (int k=j+1;k<POOLSIZE;k++) {
					for (int l=k+1;l<POOLSIZE;l++) {
						Quadruplets.add(new Quadruplet(i,j,k,l));
						// loop for going through the LottoDraw ArrayList
						//System.out.println(Quadruplets.get(Quadruplets.size()-1));
						for (int listIndex=0; listIndex < LottoDraws.size();listIndex++) {
							int [] curDraw = LottoDraws.get(listIndex).getDraw();
							/*Using boolean flags to
							 * make sure all three
							 *  numbers are in the 
							 *  same draw */
							boolean quadruplet1 = false;
							boolean quadruplet2 = false;
							boolean quadruplet3 = false;
							boolean quadruplet4 = false;
							for(int m=0;m<=DRAWSIZE;m++) {
								if (curDraw[m] == i) {
									quadruplet1 = true;
								}
								if (curDraw[m] == j) {
									quadruplet2 = true;
								}
								if (curDraw[m] == k) {
									quadruplet3 = true;
								}
								if (curDraw[m] == l) {
									quadruplet4 = true;
								}
							}
							// if the Triplet are in the draw then increment it
							// in the tripletCube
							if (quadruplet1 == true && quadruplet2 == true 
									&& quadruplet3 == true && quadruplet4 == true) {
								Quadruplets.get(Quadruplets.size()-1).incrementOccurrance();
								//System.out.println(Quadruplets.get(Quadruplets.size()-1));
							}
						}
					}
				}
			}
		}

		// The ArrayList is sorted
		Collections.sort(Quadruplets);

		for (int i=1;i<=3;i++) {
			System.out.println(Quadruplets.get(Quadruplets.size()-i) + ", ");
		}

		//System.out.println("Most Occuring Quadruplet:");

		// Sorts the arrays in to ascending order then outputs them
		//		System.out.print(Quadruplets.get(Quadruplets.size()-1) + "\n");
		//		System.out.print(Quadruplets.get(Quadruplets.size()-2) + "\n");
		//		System.out.print(Quadruplets.get(Quadruplets.size()-3) + "\n");
		System.out.println();

		Quadruplets=null;
	} // Quadruplet



	@SuppressWarnings("unchecked")
	public static void Pentuplets() {

		//long nt = System.nanoTime();

		System.out.println("Most Occuring Pentuplets:");
		ArrayList<Pent> Pents = new ArrayList<Pent>();

		// loop for going through the LottoDraw ArrayList


		// Transfers the pentuplets to the Pentuplet ArrayList
		for (int i=1;i<POOLSIZE;i++) {
			for (int j=i+1;j<POOLSIZE;j++) {
				for (int k=j+1;k<POOLSIZE;k++) {
					for (int l=k+1;l<POOLSIZE;l++) {
						for (int m=l+1;m<POOLSIZE;m++) {
							Pents.add(new Pent(i,j,k,l,m));
							for (int listIndex=0; listIndex < LottoDraws.size();listIndex++) {
								int [] curDraw = LottoDraws.get(listIndex).getDraw();
								/*Using boolean flags to
								 * make sure all three
								 *  numbers are in the 
								 *  same draw */
								boolean pentuplet1 = false;
								boolean pentuplet2 = false;
								boolean pentuplet3 = false;
								boolean pentuplet4 = false;
								boolean pentuplet5 = false;
								for(int n=0;n<=DRAWSIZE;n++) {
									if (curDraw[n] == i) {
										pentuplet1 = true;
										//System.out.println("True! i:" + i + " curDraw[n]" + curDraw[n]);
									}
									if (curDraw[n] == j) {
										pentuplet2 = true;
										//System.out.println("True! i:" + i + " curDraw[n]" + curDraw[n]);
									}
									if (curDraw[n] == k) {
										pentuplet3 = true;
										//System.out.println("True! i:" + i + " curDraw[n]" + curDraw[n]);
									}
									if (curDraw[n] == l) {
										pentuplet4 = true;
										//System.out.println("True! i:" + i + " curDraw[n]" + curDraw[n]);
									}
									if (curDraw[n] == m) {
										pentuplet5 = true;
										//System.out.println("True! :" + i + " curDraw[n]" + curDraw[n]);
									}
								}
								// if the Triplet are in the draw then increment it
								// in the tripletCube
								if (pentuplet1 == true && pentuplet2 == true 
										&& pentuplet3 == true && pentuplet4 == true
										&& pentuplet4 == true && pentuplet5 == true) {
									Pents.get(Pents.size()-1).incrementFreq();
									//System.out.println(Pents.get(Pents.size()-1));
								}
							}
						}
					}
				}
			}
		}

		// The ArrayList is sorted
		Collections.sort(Pents);

		//System.out.println("Most Occuring Pentuplet:");

		// Sorts the arrays in to ascending order then outputs them
		System.out.print(Pents.get(Pents.size()-1) + "\n");
		System.out.print(Pents.get(Pents.size()-2) + "\n");
		System.out.print(Pents.get(Pents.size()-3) + "\n");


		System.out.println();
		Pents = null;

		//System.out.println("Took " + ((System.nanoTime()-nt)/1000000000) + " seconds");

	}


	/* Score()
	 * Scores all possible numbers for best combination
	 * on the amount of money it will have won.
	 */
	@SuppressWarnings("unchecked")
	public static void Score() {
		System.out.println("Scores");
		//System.out.println(myNumbers.size());
		//System.out.println(LottoDraws.size());
		for (int myNumbersIndex=0;myNumbersIndex<=myNumbers.size()-1;myNumbersIndex++) {
			//System.out.println(myNumbers.get(myNumbersIndex));

			int [] curNums = myNumbers.get(myNumbersIndex).getNums();
			for (int listIndex=0; listIndex <= LottoDraws.size()-1;listIndex++) {
				//System.out.println(LottoDraws.get(listIndex));
				int c = 0;
				int [] curDraw = LottoDraws.get(listIndex).getDraw();
				for (int i=0;i<=DRAWSIZE;i++) {
					for (int j=0;j<=DRAWSIZE;j++) {
						if (curDraw[i] ==  curNums[j]) {
							//System.out.println("Match!! " + curNums[j]);
							//System.out.println(LottoDraws.get(listIndex));
							c++;
							//System.out.println(c);
						}
					}
				}
				//System.out.println(c);
				switch (c) {
				case 3:
					myNumbers.get(myNumbersIndex).match3();
					//System.out.println("Match!!");
					//System.out.println(LottoDraws.get(listIndex));
					//System.out.println("3");
					break;
				case 4:
					myNumbers.get(myNumbersIndex).match4();
					break;
				case 5:
					if (myNumbers.get(myNumbersIndex).getBonus()
							== LottoDraws.get(listIndex).getBonus()) {
						myNumbers.get(myNumbersIndex).matchBonus();
					} else {
						myNumbers.get(myNumbersIndex).match5();
					}
					break;
				case 6:
					myNumbers.get(myNumbersIndex).match6();
					System.out.println("Won the Lottery!");
					System.out.println(myNumbers.get(myNumbersIndex));
					System.out.println(LottoDraws.get(listIndex));
					break;
				default:
					break;
				}

			}
		}

		Collections.sort(myNumbers);

		for (int i=0;i<=myNumbers.size()-1;i++) {
			double s = myNumbers.get((myNumbers.size()-i)-1).getScore();
			double a = LottoDraws.size();
			double d = s/a;
			System.out.println(myNumbers.get((myNumbers.size()-i)-1).stats());
			System.out.println("Profit = " 
					+ (d*100) + "%");
		}

		System.out.println();
	}


	public static void main(String [] args) throws Exception {

		// Initializing the 49 balls
		for (int i=1;i<=POOLSIZE;i++) {
			balls.add(new Ball(i));
		}
		for (int i=1;i<=POOLSIZE;i++) {
			bonus.add(new Ball(i));
		}

		//System.out.println("LottoDraws.Size(" + LottoDraws.size() + ")" );

		// Reads in the results from the results file.
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String ln;

		// reads in the input from a file using a buffered reader
		while ((ln = br.readLine()) != null) {
			String[] t = ln.split(",");
			/* System.out.printf("0: " + t[0] + ", 1: " + t[1] + ", 2: " + t[2] + ",3: " + t[3] + 
			 *	",4: " + t[4] + ",5: " + t[5] + ",6: " + t[6] + ",7: " + t[7] + ",8: " 
			 *		+ t[8] + "\n");
			 */

			int draw [] = new int [6];
			for (int i=0; i <= DRAWSIZE;i++) {
				//	System.out.print("i: " + i + " t[" + (i+1) + "]: " + t[i+1] + "\n");
				draw[i] = Integer.parseInt(t[i+1]);
			}
			LottoDraws.add(new Draw(t[0],draw,Integer.parseInt(t[7]),t[8]));
			//TODO Add bonus ball
			//System.out.println(LottoDraws.get(LottoDraws.size()-1).toString());
		}

		//System.out.println("LottoDraws.Size(" + LottoDraws.size() + ")" );


		Occurances();
		MostOccuring();
		//Score();

		System.out.println("Press 2 for pairs");
		System.out.println("Press 3 for triplets");
		System.out.println("Press 4 for quadruplets");
		System.out.println("Press 5 for pentuplets");


		do {
			int r = System.in.read() ;

			switch (r) {
			case 50:
				pairs();
				System.gc();
				break;
			case 51:
				Triplets();
				System.gc();
				break;
			case 52:
				Quadruplets();
				System.gc();
				break;
			case 53:
				Pentuplets();
				System.gc();
				break;
			case 48:
				Score();
				System.out.println("Done.");
				System.exit(0);
			default:
				System.out.println("You broke me :(");
				break;
			}

		} while (true);


	}

}

