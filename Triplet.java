
public class Triplet implements Comparable{
	private String name;
	private int occ;
	private int num1;
	private int num2;
	private int num3;

	public Triplet(int n, int m, int o) {
		if (n==m || n==o || m==o) {
			System.out.println("Triplet: " + n + m + o + 
					" Something fucked up with the naming of this pair!");
		}
		if (n < 10) {
			this.name = "0" + Integer.toString(n);
		} else {
			this.name = Integer.toString(n);
		}
		
		if (m < 10 ) {
			this.name = this.name + "0" + Integer.toString(m);
		} else { 
			this.name = this.name + Integer.toString(m);
		}
		
		if (o < 10 ) {
			this.name = this.name + "0" + Integer.toString(o);
		} else { 
			this.name = this.name + Integer.toString(o);
		}
		
		this.num1 = n;
		this.num2 = m;
		this.num3 = o;
		this.occ = 0;
	}

	public String getName() {
		return name;
	}

	public int getOccurance() {
		return occ;
	}

	public String toString(){
		return (name.substring(0,2) + " " + name.substring(2, 4) + " " + name.substring(4, 6) + "(" + occ +")");
	}
	
	public int getNum1() {
		return num1;
	}
	
	public int getNum2() {
		return num2;
	}
	
	public int getNum3() {
		return num3;
	}
	
	public void incrementOccurrance() {
		this.occ++;
	}
	
	public int compareTo(Object anotherTriplet) throws ClassCastException {
	    if (!(anotherTriplet instanceof Triplet))
	      throw new ClassCastException("A Triplet object expected.");
	    int anotherTripletOccurance = ((Triplet) anotherTriplet).getOccurance();  
	    return this.occ - anotherTripletOccurance;    
	  }
}
