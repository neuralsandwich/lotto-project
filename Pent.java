
public class Pent implements Comparable{
	private String name;
	private int num1;
	private int num2;
	private int num3;
	private int num4;
	private int num5;
	private int freq;

	public Pent(int n, int m, int o, int p, int q) {
		if (n==m || n==o || m==o || o==p || p==n || p==m || p==q
				|| q==m || q==n || q==o) {
			System.out.println("Pent: " + n + m + o + 
					" Something fucked up with the naming of this pair!");
		}
		if (n < 10) {
			this.name = "0" + Integer.toString(n);
		} else {
			this.name = Integer.toString(n);
		}
		
		if (m < 10 ) {
			this.name = this.name + "0" + Integer.toString(m);
		} else { 
			this.name = this.name + Integer.toString(m);
		}
		
		if (o < 10 ) {
			this.name = this.name + "0" + Integer.toString(o);
		} else { 
			this.name = this.name + Integer.toString(o);
		}
		
		if (p < 10 ) {
			this.name = this.name + "0" + Integer.toString(p);
		} else { 
			this.name = this.name + Integer.toString(p);
		}
		
		if (q < 10 ) {
			this.name = this.name + "0" + Integer.toString(q);
		} else { 
			this.name = this.name + Integer.toString(q);
		}
		
		this.num1 = n;
		this.num2 = m;
		this.num3 = o;
		this.num4 = p;
		this.num5 = q;
		this.freq = 0;
	}

	public String getName() {
		return name;
	}

	public String toString(){
		return (name.substring(0,2) + " " + name.substring(2, 4) 
				+ " " + name.substring(4, 6) + " " + name.substring(6, 8) + " " + name.substring(8, 10) + "(" + freq +")");
	}
	
	public int getNum1() {
		return num1;
	}
	
	public int getNum2() {
		return num2;
	}
	
	public int getNum3() {
		return num3;
	}
	
	public int getNum4() {
		return num4;
	}
	
	public int getNum5() {
		return num5;
	}
	
	public int getFreq() {
		return freq;
	}
	
	public void incrementFreq() {
		this.freq++;
	}
	
	public int compareTo(Object anotherPent) throws ClassCastException {
	    if (!(anotherPent instanceof Pent))
	      throw new ClassCastException("A pent object expected.");
	    int anotherPentOccurrance = ((Pent) anotherPent).getFreq();  
	    return this.freq - anotherPentOccurrance;    
	  }
}
