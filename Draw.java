import java.util.Arrays;

public class Draw {

	private String date;
	private int [] draw;
	//TODO implement bonus ball
	private int bonus;
	private String machine;
	
	public Draw(String date, int [] d, int b, String m) {
		this.date = date;
		this.draw = d;
		this.bonus = b;
		this.machine = m;
		Arrays.sort(d);
	}
	
	public String getDate() {
		return this.date;
	}
	
	public int [] getDraw() {
		return this.draw;
	}
	
	public int getBonus() {
		return this.bonus;
	}
	
	public String getMachine() {
		return this.machine;
	}
	
	public String toString() {
		String str = date;
		for (int i=0;i<=5;i++) {
			str = str + ", " + Integer.toString(draw[i]);
		}
		str = str + ", " + machine;
		return str;
	}
	
}