
public class Ball implements Comparable {

	private int num;
	private int freq;
	
	public Ball(int n) {
		this.num = n;
		this.freq = 0;
	}
	
	public int getNum() {
		return num;
	}
	
	public int getFreq() {
		return freq;
	}
	
	public void incrementFreq() {
		this.freq++;
	}
	
	public String toString() {
		return (num + "(" + freq + ")");
	}

	public void setNum(int n) {
		this.num = n;
	}

	@Override
	public int compareTo(Object anotherBall) throws ClassCastException {
		if (!(anotherBall instanceof Ball))
		      throw new ClassCastException("A Pair object expected.");
		    int anotherBallFreq = ((Ball) anotherBall).getFreq();  
		    return this.freq - anotherBallFreq;   
	}
}
