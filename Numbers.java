
public class Numbers implements Comparable {

	private String name;
	private int num1;
	private int num2;
	private int num3;
	private int num4;
	private int num5;
	private int num6;
	private int [] nums = new int [6];
	private int score;
	private int three;
	private int four;
	private int five;
	private int bonus;
	private int six;

	public Numbers(int [] a) {
		this.nums = a;
		this.num1 = a[0];
		this.num2 = a[1];
		this.num3 = a[2];
		this.num4 = a[3];
		this.num5 = a[4];
		this.num6 = a[5];
		this.score=0;
		this.three=0;
		this.four=0;
		this.five=0;
		this.six=0;
		this.name="";
		for (int i=0;i<=5;i++) {
			if (a[i] < 10) {
				this.name = this.name + "0" + Integer.toString(a[i]);
				//System.out.println("0" + Integer.toString(a[i]));
			} else {
				this.name = this.name + Integer.toString(a[i]);
				//System.out.println(Integer.toString(a[i]));
			}
		}
		//System.out.println(name);
	}

	public String getName() {
		return name;
	}

	public String toString(){
		return (name.substring(0,2) + " " + name.substring(2,4) 
				+ " " + name.substring(4, 6) + " " + name.substring(6, 8) 
				+ " " + name.substring(8, 10) + " " + name.substring(10, 12) 
				+ "(" + score +")");
	}

	public int getNum1() {
		return num1;
	}

	public int getNum2() {
		return num2;
	}

	public int getNum3() {
		return num3;
	}

	public int getNum4() {
		return num4;
	}

	public int getNum5() {
		return num5;
	}

	public int getNum6() {
		return num6;
	}
	
	public int getBonus() {
		return bonus;
	}

	public int [] getNums() {
		return nums;
	}

	public int getScore() {
		return score;
	}

	public void match3() {
		this.three++;
		score = score + 10;
	}

	public void match4() {
		this.four++;
		score = score + 50;
	}

	public void match5() {
		this.five++;
		score = score + 1000;
	}
	
	public void matchBonus() {
		this.bonus++;
		score = score + 100000;
	}

	public void match6() {
		this.six++;
		score = score + 500000;
	}
	
	public String stats() {
		return (name.substring(0,2) + " " + name.substring(2,4) 
				+ " " + name.substring(4, 6) + " " + name.substring(6, 8) 
				+ " " + name.substring(8, 10) + " " + name.substring(10, 12) 
				+ "(" + score +")" + "\n"
				//Stats
				+ "3 ball matches: " + three + "\n"
				+ "4 ball matches: " + four + "\n"
				+ "5 ball matches: " + five + "\n"
				+ "6 ball matches: " + six
				);
	}

	public int compareTo(Object anotherNumbers) throws ClassCastException {
		if (!(anotherNumbers instanceof Numbers))
			throw new ClassCastException("A pent object expected.");
		int anotherNumbersScore = ((Numbers) anotherNumbers).getScore();  
		return this.score - anotherNumbersScore;    
	}
}

