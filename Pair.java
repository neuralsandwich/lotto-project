
public class Pair implements Comparable {
	private String name;
	private int occ;
	private int num1;
	private int num2;

	public Pair(int n, int m) {
		if (n==m)
			System.out.println("pair: " + n + m + " Something fucked up with the naming of this pair!");
		if (n < 10) {
			this.name = "0" + Integer.toString(n);
		} else {
			this.name = Integer.toString(n);
		}
		
		if (m < 10 ) {
			this.name = this.name + "0" + Integer.toString(m);
		} else { 
			this.name = this.name + Integer.toString(m);
		}
		
		this.num1 = n;
		this.num2 = m;
		this.occ = 0;
	}

	public String getName() {
		return name;
	}

	public int getOccurance() {
		return occ;
	}

	public String toString(){
		return (name.substring(0,2) + " " + name.substring(2, 4) + "(" + occ +")");
	}
	
	public int getNum1() {
		return num1;
	}
	
	public int getNum2() {
		return num2;
	}
	
	public void incrementOccurrance() {
		this.occ++;
	}
	
	public int compareTo(Object anotherPair) throws ClassCastException {
	    if (!(anotherPair instanceof Pair))
	      throw new ClassCastException("A Pair object expected.");
	    int anotherPairOccurance = ((Pair) anotherPair).getOccurance();  
	    return this.occ - anotherPairOccurance;    
	  }

}
